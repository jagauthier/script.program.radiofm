import xbmcaddon, xbmcgui, xbmc
import threading
import time
import anydbm
import os
import socket,errno
import textwrap

__addon__     = xbmcaddon.Addon()
# The path to the addon
__addonpath__ = __addon__.getAddonInfo('path').decode("utf-8")
# The path to the userdata
__addondir__    = xbmc.translatePath( __addon__.getAddonInfo('profile') )

#global used to tell the worker thread the status of the window
__windowopen__  = True

# This will be used to determine local/remote calls
__radiolocal__=True

radio_dialog=None

#capture a couple of actions to close the window
ACTION_PREVIOUS_MENU = 10
ACTION_MOUSE_LONG_CLICK = 108
ACTION_CONTEXT_MENU = 117

ACTION_MOUSE_LEFT_CLICK = 100

ACTION_BACK = 92

BACK_BTN     = 1501
TITLE_LABEL  = 1502
FM1_UP_BTN   = 1503
FM1_DN_BTN   = 1504
FM2_UP_BTN   = 1505
FM2_DN_BTN   = 1506

FLARE_BTN    = 1509

BUTTONS      = 5
# These are the actual buttons for stored stations
BASE_BTN     = 1510
# These are the empty stars
BASE_REG     = 1520
# These are when the filled in stars
BASE_HIGH    = 1525
# These are the label for the buttons
BASE_LABEL   = 1530

FMSIGNAL_IMG = 1550
STEREO_LBL   = 1551

FMBAND_IMG   = 1552
FMBAND_LBL   = 1553

TUNE_LFT_BTN = 1560
TUNE_RGT_BTN = 1561

BASE_SEEKTH  = 1570
SEEKTH_BTN   = 1575

# Number of seekth settings/options
SEEKTHS      = 4

FAV_DB=__addondir__+"favorites.db"

# Will change these if remote
REMOTE_IP="home.pendulus.org"
REMOTE_IP="localhost"
BUFFER_SIZE=1024

REMOTE_PORT=4703

TCP_IP='127.0.0.1'
TCP_PORT=4703

BUFFER_SIZE=128

current_milli_time = lambda: int(round(time.time() * 1000))

variables={}

def log(logline):
    try:
        print "RADIOXML: " + logline
    except:
        pass
    
def set_kodi_prop(property, value):
    global radio_dialog
    global __radiolocal__
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)
    xbmcgui.Window(10000).setProperty(property, strvalue)
    
    if __radiolocal__==False:
        radioFM.send_network(radio_dialog, "set_kodi_prop " +  property + " " + strvalue+"\n")

# Only used if doing the TCP IP stuff. I use this function (set_kodi_prop) too much
# to change the definition
def set_kodi_prop_lcl(property, value):
#    log ("Setting: '{}', '{}'".format(property, value))
    strvalue=str(value)    
    xbmcgui.Window(10000).setProperty(property, strvalue)
                                        
def get_kodi_prop(property):
    global __radiolocal__
    global radio_dialog
    value=xbmcgui.Window(10000).getProperty(property)    
    if __radiolocal__==False and value=="":
        radioFM.send_network(radio_dialog, "get_kodi_prop " +  property + "\n")
        # actually set it locally too
    log("Loading: '" + property + "', '" + value + "'")        
    return value


def update_display():
    global radio_dialog    
    
    freq=get_kodi_prop("radio_freq")
    freq=freq.split("\n")[0].rstrip()
    callsign=get_kodi_prop("radio_callsign")
    
    rssi=get_kodi_prop("radio_rssi")
    rssi=rssi.split("\n")[0].rstrip()
    st=get_kodi_prop("radio_st")

    city=get_kodi_prop("radio_city")
    state=get_kodi_prop("radio_state")
    miles=get_kodi_prop("radio_miles")
    
    pty=get_kodi_prop("radio_pty")
    
    ps=get_kodi_prop("radio_ps")
    radio_text=get_kodi_prop("radio_text")
    radio_text=radio_text.rstrip()
    
    # if over the network we haven't received the variables yet
    if freq=="":
        return
    
    if radio_text!="" and "[CR]" not in radio_text:
        txt_w=textwrap.wrap(radio_text,28)
        new_txt=""
        for t in txt_w:
            new_txt=new_txt+t+"[CR]"
        set_kodi_prop_lcl("radio_text_fmt", new_txt)

    if st=="1":
        radioFM.stereo_lbl.setLabel("[COLOR white]STEREO[/COLOR]")
    else:
        radioFM.stereo_lbl.setLabel("[COLOR black]STEREO[/COLOR]")
        
    # 75 is the max DB from the si4703

    rssi_perc=int(rssi)*100/70
    if rssi_perc<25:
        radioFM.fmsignal_img.setImage("FMsignal25.png")
    elif rssi_perc<50:
        radioFM.fmsignal_img.setImage("FMsignal50.png")
    elif rssi_perc<75:
        radioFM.fmsignal_img.setImage("FMsignal75.png")
    else:
        radioFM.fmsignal_img.setImage("FMsignal100.png")

    #set the position of the fmband marker based on the current frequency
    # we'll get the FMband image info to calculate it
        
    startx=radioFM.fmband_img.getX()-27 ## All of this math and I still had to shift it 27 pixels left
    width=radioFM.fmband_img.getWidth()-10
    # total length of the FM band
    total_pix=startx+width
    # There are 100 bands between 87.9 and 107.9
    # The percentage of the freq to the channel range, in decimal
    freq_perc=(float(freq)-87.9)/20
    # Now just figure out the appropriate X position
    radioFM.fmband_lbl.setPosition(startx+int(width*freq_perc),radioFM.fmband_lbl.getY())
    
    # And here we are going to run through the flare feature in case tuning happened,
    # we don't really know!
    radioFM.flare_button(radio_dialog, freq)
    
    # And the seek display
    radioFM.draw_seekth(radio_dialog)

    
def updateWindow():
    global __windowopen__
    # I put this here because sometimes the thread starts before the onInit function
    # is complete. When that happens, it throws an exception.
    # We can assume that once statusbar is not None, it's done enough
    while radioFM.title_label==None:
        time.sleep(.25)
                                
    
    #this is the worker thread that updates the window information every w seconds
    #this strange looping exists because I didn't want to sleep the thread for very long
    #as time.sleep() keeps user input from being acted upon

    while __windowopen__ and (not xbmc.abortRequested):
        bms=current_milli_time()
        update_display()
        ams=current_milli_time()
        # give us a break
        time.sleep(1)

# store the favorite. Create DB if necassary
def store_favorite(index, favorite):
    if not os.path.exists(__addondir__):
        os.makedirs(__addondir__)
                            
    db=anydbm.open(FAV_DB, "c")
    db[str(index)]=favorite
    radioFM.faved_stations[index]=favorite
    db.close

def remove_favorite(index, favorite):
                            
    db=anydbm.open(FAV_DB, "c")
    del db[str(index)]
    radioFM.faved_stations[index]=""
    db.close

# get the favorite. DB doesn't exist, or the key, return blanks
def get_favorite(index):
    fave=""
    try:
        db=anydbm.open(FAV_DB, "r")        
        fave=db[str(index)]
    except:
        pass
    return fave
    db.close

    
class radioFM(xbmcgui.WindowXMLDialog):
    
    title_label=None
    fm1_up_btn=None
    fm1_dn_btn=None
    fm2_up_btn=None
    fm2_dn_btn=None
    
    stereo_lbl=None
    fmsignal_img=None

    fmband_img=None
    fmband_lbl=None
    
    flare_btn=None
    button_offset=0
    
    sock = None
    faved_stations=[0 for x in range(BUTTONS*2)]

    timeout=.25
    clicker=0
    target_control=0
    
    def __init__(self,strXMLname, strFallbackPath, strDefaultName, forceFallback):
        log("Initializing...")
        #If this is not set, then the manager service is not running in kodi
        global TCP_IP
        global TCP_PORT
        global __radiolocal__
        
        if get_kodi_prop("kodi_manager")=="":
            log("Radio is not local")
            __radiolocal__=False
            TCP_IP=REMOTE_IP
            TCP_PORT=REMOTE_PORT
            
        log ("Starting network.")
        threading.Thread(target=self.start_network).start()
           
    def network_response(self,data):
        tune=False
        data_split=data.split("\n")
        
        # loop through the data
        for data_line in data_split:
            request=data_line.split(" ")
            if request[0]=="set_kodi_prop":
                if len(request)<3:
                    return
                set_kodi_prop_lcl(request[1], " ".join(request[2:]))
                
#        log("Received: " + data)
            
    def start_network(self):
        global __windowopen__        
        # This is the network client handler
        # used mostly for sending radio commands
        # But if the manager service and radio are running on the command line
        # then it will be used to pass kodi variables as well

        server_address = (TCP_IP, TCP_PORT)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(server_address)
        
        while __windowopen__ == True:
            data = self.sock.recv(BUFFER_SIZE)
            if not data:
                log("Closing socket")
                self.sock.close()
                return
            self.network_response(data)
        self.sock.close()
            
    def send_network(self, data):
        try:
            self.sock.send(data)
        except:
            raise
        
    def onInit(self):
        set_kodi_prop("radio_active","1")
        # This is for the fancy looking button so we know which station is active
        radioFM.flare_btn=self.getControl(FLARE_BTN)
        radioFM.flare_btn.setEnabled(False)
        radioFM.flare_btn.setVisible(False)
        
        radioFM.fm1_up_btn=self.getControl(FM1_UP_BTN)
        radioFM.fm1_dn_btn=self.getControl(FM1_DN_BTN)
        radioFM.fm2_up_btn=self.getControl(FM2_UP_BTN)
        radioFM.fm2_dn_btn=self.getControl(FM2_DN_BTN)
    
        radioFM.fmsignal_img=self.getControl(FMSIGNAL_IMG)
        radioFM.stereo_lbl=self.getControl(STEREO_LBL)
        
        radioFM.fmband_img=self.getControl(FMBAND_IMG)
        radioFM.fmband_lbl=self.getControl(FMBAND_LBL)
        
        # set the default enablement for the two FM buttons
        radioFM.fm1_up_btn.setVisible(False)
        radioFM.fm1_up_btn.setEnabled(False)
        radioFM.fm1_dn_btn.setVisible(True)
        radioFM.fm1_dn_btn.setEnabled(True)        

        radioFM.fm2_up_btn.setVisible(True)
        radioFM.fm2_up_btn.setEnabled(True)        
        radioFM.fm2_dn_btn.setVisible(False)
        radioFM.fm2_dn_btn.setEnabled(False)
        
        # setup the favorite icons
        for i in range(BUTTONS):
            reg=self.getControl(BASE_REG+i)
            reg.setVisible(True)
            reg.setEnabled(True)            
            high=self.getControl(BASE_HIGH+i)
            high.setVisible(False)
            high.setEnabled(False)
            # set the labels to blanks, we'll configure them shortly
            label=self.getControl(BASE_LABEL+i)
            label.setLabel("")
            
#       set_kodi_prop("radio_freq", "107.7")
        freq=get_kodi_prop("radio_freq")
        
        # load the favorites into an array
        # tab is FM1 or FM2, default to FM1 in case of zero matches
        tab=FM1_UP_BTN
        for i in range(BUTTONS*2):
            self.faved_stations[i]=get_favorite(str(i))
            # check if the current station is a loaded favorite
            if self.faved_stations[i].split(" ")[0].rstrip()==freq:
                # if i<BUTTONS then the current is in panel FM1
                if i<BUTTONS:
                    tab=FM1_UP_BTN
                else:
                    tab=FM2_UP_BTN
         
        # Now switch to that panel where we'll do the rest
        self.onClick(tab)
        
        # This is loaded last, because we use it in the main looping thread
        # to make sure this function has completed initialization
        radioFM.title_label=self.getControl(TITLE_LABEL)

        
    def onAction(self, action):
        global __windowopen__
        actionID=action.getId()
        controlID=self.getFocusId()
        log ("Action: " + str(actionID) + " /Control: " + str(controlID))        

        
        if action == ACTION_PREVIOUS_MENU:
            log("ENDING. prev menu")
            __windowopen__ = False
            self.close()

        if action == 107: 
            return

#        log("Action : " + str(actionID))
#        log("Control: " + str(controlID))
        
        if actionID in [ACTION_CONTEXT_MENU, ACTION_MOUSE_LONG_CLICK]:
            if controlID == TUNE_LFT_BTN:
                self.seek("left")
            elif controlID == TUNE_RGT_BTN:
                self.seek("right")

    def onClick(self, controlID):
        global __windowopen__        
        log("On click: " + str(controlID))

        if controlID == FM1_UP_BTN or controlID == FM2_UP_BTN:
            freq=get_kodi_prop("radio_freq")
            # FM1 button was pressed
            if controlID == FM1_UP_BTN:
                self.button_offset=0
                radioFM.fm1_up_btn.setVisible(False)
                radioFM.fm1_up_btn.setEnabled(False)
                radioFM.fm1_dn_btn.setVisible(True)
                radioFM.fm1_dn_btn.setEnabled(True)        
                
                radioFM.fm2_up_btn.setVisible(True)
                radioFM.fm2_up_btn.setEnabled(True)        
                radioFM.fm2_dn_btn.setVisible(False)
                radioFM.fm2_dn_btn.setEnabled(False)
            else:
                self.button_offset=BUTTONS
                radioFM.fm1_up_btn.setVisible(True)
                radioFM.fm1_up_btn.setEnabled(True)
                radioFM.fm1_dn_btn.setVisible(False)
                radioFM.fm1_dn_btn.setEnabled(False)        
                
                radioFM.fm2_up_btn.setVisible(False)
                radioFM.fm2_up_btn.setEnabled(False)        
                radioFM.fm2_dn_btn.setVisible(True)
                radioFM.fm2_dn_btn.setEnabled(True)
            
            # load up the labels with faves
            for i in range(BUTTONS):
                # set the label
                label=self.getControl(BASE_LABEL+i)
                
                label.setLabel(self.faved_stations[i+self.button_offset])
                # make the star lit up
                if label.getLabel()!="":
                    reg=self.getControl(BASE_REG+i)
                    reg.setVisible(False)
                    reg.setEnabled(False)            
                    high=self.getControl(BASE_HIGH+i)
                    high.setVisible(True)
                    high.setEnabled(True)
                else:
                    # make the star not light up -- kind of a reset
                    reg=self.getControl(BASE_REG+i)
                    reg.setVisible(True)
                    reg.setEnabled(True)            
                    high=self.getControl(BASE_HIGH+i)
                    high.setVisible(False)
                    high.setEnabled(False)

            # a little flare for the current button                    
            self.flare_button(freq)
            
        if controlID >= BASE_BTN and controlID <= BASE_BTN+BUTTONS:
            # we'll need to do some tuning, if there is a label.
            # If not, we'll ignore the button press
            # This is figuring out the control id of the label based on the 
            # base of each. It's 20 (1530-1510), but if we want to change these
            # by adding/removing buttons then we'll still be accurate
            button_lbl=self.getControl((BASE_LABEL-BASE_BTN)+controlID)
            if button_lbl.getLabel()=="":
                return

            new_freq=button_lbl.getLabel().split(" ")[0].rstrip()
            self.tune(new_freq)
            # Update the button label with new information, eh
            button=self.getControl(controlID+(BASE_LABEL-BASE_BTN))
            callsign=get_kodi_prop("radio_callsign")
            button.setLabel(new_freq+ " " + callsign)
            self.flare_button(new_freq)
            store_favorite(controlID-BASE_BTN+self.button_offset,new_freq+" "+callsign)
             
        if controlID >= BASE_REG and controlID < BASE_REG+BUTTONS:
            # Loop through the existing labels. If we're already set then ignore this
            for i in range(BUTTONS):
                button_lbl=self.getControl(BASE_LABEL+i)
                freq_btn=button_lbl.getLabel().split(" ")[0].rstrip()                
                # button favorite exists, so ignore this and leave
                if freq_btn==get_kodi_prop("radio_freq"):
                    return
            fav_reg=self.getControl(controlID)
            fav_high=self.getControl(controlID+BUTTONS)
            fav_reg.setVisible(False)
            fav_reg.setEnabled(False)
            fav_high.setVisible(True)
            fav_high.setEnabled(True)
            # now get the button itself
            button=self.getControl((BASE_REG-BASE_BTN)+controlID)
            freq=get_kodi_prop("radio_freq")
            callsign=get_kodi_prop("radio_callsign")
            button.setLabel(freq+ " " + callsign)
            self.flare_button(freq)
      
            store_favorite(controlID-BASE_REG+self.button_offset,freq+" "+callsign)
            
        if controlID >= BASE_HIGH and controlID < BASE_HIGH+BUTTONS:
            fav_high=self.getControl(controlID)
            fav_reg=self.getControl(controlID-BUTTONS)
            fav_high.setVisible(False)
            fav_high.setEnabled(False)
            fav_reg.setVisible(True)
            fav_reg.setEnabled(True)
            button=self.getControl(controlID+(BASE_LABEL-BASE_HIGH))
            
            button.setLabel("")
            freq=get_kodi_prop("radio_freq")
            self.flare_button(freq)
            remove_favorite(controlID-BASE_HIGH+self.button_offset, freq)

        if controlID == SEEKTH_BTN:
            seekth=int(get_kodi_prop("radio_seekth"))
            base_seekth=int(get_kodi_prop("radio_base_seekth"))
            seekth=seekth+1
            if seekth>=base_seekth+4:
                seekth=base_seekth
            self.send_network("RADIO SEEKTH " + str(seekth) + "\n")
            
        if controlID == BACK_BTN:
            global __windowopen__            
            log("ENDING. back button")
            __windowopen__ = False
            self.close()

        if controlID == TUNE_LFT_BTN:
            self.tune_left()
            
        if controlID == TUNE_RGT_BTN:
            self.tune_right()
        
        if controlID == FMBAND_IMG:
            # Now calculate the frequency based on where we clicked
            # This is the math to get the station
            station=radioFM.fmband_img.getPercent()*.2+87.9
            self.tune(station)

    def flare_button(self, radio_freq):
        # Here's how this will work.  First we enabled all the buttons, and make them visible
        # Because it's possible, that we previously disabled one (or more).
        # But we don't know which ones
        # Then loop through the button an<d flare the one that is the same as the station we are 
        # listening to
        radioFM.flare_btn.setVisible(False)
        
        for i in range(BUTTONS):
            this_button=self.getControl(BASE_BTN+i)
            # enable
            this_button.setEnabled(True)
            this_button.setVisible(True)
            # get the label
            button_lbl=self.getControl(BASE_LABEL+i)
            freq_btn=button_lbl.getLabel().split(" ")[0].rstrip()
            if freq_btn==radio_freq:
                # now disable
                this_button.setEnabled(False)
                this_button.setVisible(False)                
                radioFM.flare_btn.setPosition(this_button.getX(),this_button.getY())
                radioFM.flare_btn.setVisible(True)

    def draw_seekth(self):
        
        seekth=get_kodi_prop("radio_seekth")
        base_seekth=get_kodi_prop("radio_base_seekth")
        if seekth=="" or base_seekth=="":
            return
        seekth=int(seekth)
        base_seekth=int(base_seekth)
        for i in range(0, SEEKTHS):
            btn=self.getControl(BASE_SEEKTH+i)
            btn.setImage("seekthgrey.png")
        for i in range(0, seekth-base_seekth+1):
            if seekth-i>=base_seekth:
                btn=self.getControl(BASE_SEEKTH+i)
                btn.setImage("seekthblue.png")
        
    def onFocus(self, controlID):
        pass
    
    def onControl(self, controlID):
        pass

    def tune(self, freq):
        self.send_network("RADIO TUNE " + str(freq) + "\n")
        time.sleep(.1)

    def seek(self, direction):

        while 1:
            old_freq=get_kodi_prop("radio_freq")
            log("LEFT SEEKING FROM " + old_freq)
            if "left" in direction:
                self.tune_left()
            else:
                self.tune_right()
            count=0            
            match=False
            while match==False:
                new_freq=get_kodi_prop("radio_freq")
                if old_freq!=new_freq:
                    log("Old: " + old_freq + " New: " + new_freq)
                    match=True
                
                time.sleep(.1)
                count=count+1
                if count>100:
                    log("Leaving. Fail safe.")
                    return

            log("STATION CHANGED TO " + new_freq)
            rssi=get_kodi_prop("radio_rssi")
            seekth=get_kodi_prop("radio_seekth")
            log("RSSI: " + rssi + " SEEKTH: " + seekth)
            if int(rssi)>=int(seekth):
                break

    def tune_left(self):
        freq=float(get_kodi_prop("radio_freq"))
        freq=freq-0.2
        if freq<87.9:
            freq=107.9
        self.tune(freq)

    def tune_right(self):
        freq=float(get_kodi_prop("radio_freq"))
        freq=freq+.2
        if freq>107.9:
            freq=87.9
        self.tune(freq)
    
if  __name__ == '__main__':    
 
    t1 = threading.Thread( target=updateWindow)
    t1.setDaemon( True )
    t1.start()
 
    radio_dialog = radioFM("radioFM.xml", __addonpath__, 'default', '720p')
    radio_dialog.doModal()
    del radio_dialog
 